if (whoami != root)
  then echo "Please run as root"

  else (
  
  echo "Please wait, we install the outbuildings for you :)"
  sudo apt-install curl
  echo "We just installed curl"
  sudo curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
  echo "We just copied the files to install node.js and npm"
  sudo apt install nodejs
  echo "Here we go, node.js and npm are installed."
  echo "Now we're going to set things up so that we can send the text messages"
  sudo npm install -g json
  echo "It's almost the end"
  echo "Just one more second"
  echo "There you go, didn't even have time to make a cup of coffee and it's already over."
  
  
  

    )
fi

exit
