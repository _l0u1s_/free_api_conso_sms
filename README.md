# Free API consomation  
[![Build Status](https://travis-ci.com/louisgallet95/free_api_conso_sms.svg?branch=master)](https://travis-ci.com/louisgallet95/free_api_conso_sms)
This is a small application to be notified in direct time by sms of its consumption.

# Installation
To install the service, clone or download a release of github. Then run the install.sh script. At the end of the installation modify the file test_api.sh and enter your login. 
Once the installation is complete, test the service by running the test_api.sh file.

#### This script requires the activation of the SMS notification service on your Free Mobile account.


Test
